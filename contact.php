<?php session_start();?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Contact Us</title>
<?php include './include/head.php'; ?>
</head>

<style>

</style>

<link href="styles/contactus.css" rel="stylesheet" type="text/css" media="screen" />

<body>

<!-- Menu -->

<div class="menu">
	<?php include './include/menu_mobile.php'; ?>
</div>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<?php include './include/header.php'; ?>
	</header>

	<div class="super_container_inner">
		<div class="super_overlay"></div>
		
		<div class="boxes">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="boxes_container d-flex flex-row align-items-start justify-content-between flex-wrap">
							<!-- Box -->
							<div style="margin-top:100px !important; background-color:#2fce98 !important; height:100px; width:100%; color:white; text-align:center; padding:10px;">
								<h3>CONTACT US</h3>	
								<p style="color:white;">If you need to get in touch with us, you can drop by our office during the working hours.</p>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
			
		<!-- Boxes -->

		<div class="boxes">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="boxes_container" style="height:500px;">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d294.8885806845057!2d70.76760655765712!3d22.309712321709654!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3959c98328e4accb%3A0xbace9a141e83f6ba!2sJay%20Ganesh%20Complex%2C%20Dharamnagar%2C%20150%20Feet%20Ring%20Rd%2C%20Dharam%20Nagar%2C%20Rajkot%2C%20Gujarat%20360005!5e0!3m2!1sen!2sin!4v1589028280017!5m2!1sen!2sin" width="100%" height="450" frameborder="2" style="border:2;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Features -->

		<div class="features">
			<div class="container">
				<div class="row">
					
					<div class="contactUsWrap">
						
						<div class="mapWrapper">
						<div class="map" id="map" style="position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-err-container"><div class="gm-err-content"><div class="gm-err-icon"><img src="https://maps.gstatic.com/mapfiles/api-3/images/icon_error.png" draggable="false" style="user-select: none;"></div><div class="gm-err-title">Oops! Something went wrong.</div><div class="gm-err-message">This page didn't load Google Maps correctly. See the JavaScript console for technical details.</div></div></div></div></div>
						</div>
						<div class="officeAddressWrap">
						<div class="container">
							<div class="heading">OFFICE ADDRESS</div>
							<div class="desc"> JAY GANESH COMPLEX, SHOP NO 8, NR SADBHAVNA HOSPITAL, DHARAMNAGAR, 150 FT RING ROAD, RAJKOT – 360005, (GUJARAT), (INDIA)</div>
							<div class="itemsWrap">
							<ul>
								<li>
								<div class="label">MOBILE NO</div>
								<div class="details">+91 7622020893</div>
								</li>
								<li>
								<div class="label">WHATSAPP NO</div>
								<div class="details">+91 7622020893</div>
								</li>
								<li>
								<div class="label">E-MAIL</div>
								<div class="details"><a href="mailto:shree.ent1992@gmail.com">shree.ent1992@gmail.com</a></div>
								</li>
								<li>
								<div class="label">HOURS</div>
								<div class="details">9.30am - 8.00pm</div>
								</li>
							</ul>
							</div>
						</div>
						</div>
						<div>
							
						
						</div>
					</div>

					<div class="col-lg-4 feature_col" style="align:center; width:200px;">
							&nbsp;
					</div>
					
					<div class="col-lg-4 feature_col" style="align:center; width:200px;">
						<form action="add_contact.php" method="post" enctype="multipart/form-data">

								<h2 style="text-align:center;">CONTACT FORM</h2>

								<div class="input-container">
									<i class="fa fas fa-user-circle icon" style="font-size:30px" ></i>
									<input class="input-field" type="text" placeholder="Your Name" name="uc_name">
								</div>

								<div class="input-container">
									<i class="fa fa-envelope icon" style="font-size:30px"></i>
									<input class="input-field" type="text" placeholder="Email" name="uc_email">
								</div>

								<div class="input-container">
									<i class="fa fa-mobile-phone icon" style="font-size:30px"></i>
									<input class="input-field" type="text" placeholder="Mobile No" name="uc_mno">
								</div>

								<div class="input-container">
									<i class="fa fa-home icon" style="font-size:30px"></i>
									<textarea name="uc_address" class="input-field"  placeholder="Address"></textarea>
								</div>

								<div class="input-container">
									<i class="fa fa-bank icon" style="font-size:30px"></i>
									<input class="input-field" type="text" placeholder="City" name="uc_city">
								</div>
								
								<div class="input-container">
									<i class="fas fa-place-of-worship icon" style="font-size:30px"></i>
									<input class="input-field" type="text" placeholder="State" name="uc_state">
								</div>

								<div class="input-container">
									<i class="fa fa-key icon" style="font-size:30px"></i>
									<input class="input-field" type="text" placeholder="Pincode" name="uc_pincode">
								</div>

								<div class="input-container">
									<i class="fa fa-key icon" style="font-size:30px"></i>
									<textarea name="uc_message" class="input-field"  placeholder="Write Your Message here"></textarea>
								</div>

								<button type="submit" class="btn">Submit</button>

								<?php 

									if(isset ($_SESSION['add_contact']['ok']))
									{
										echo '<font color="blue">'.($_SESSION['add_contact']['ok']).'</font></br>';
									}

									if(!empty($_SESSION['add_contact']['error']))
									{
										echo '<font color="red">'.($_SESSION['add_contact']['error']).'</font></br>';
									}
									
									unset($_SESSION['add_contact']);
								?>

							</form>
					</div>

				</div>
			</div>
		</div>

		<!-- Footer -->
		<?php include './include/footer.php'; ?>
		
	</div>
		
</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap-4.1.2/popper.js"></script>
<script src="styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/progressbar/progressbar.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>