<?php session_start(); 

if(!isset($_SESSION['unm']))
{
	header("location:login.php");
}?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Admin</title>
<style type="text/css">

</style>
<link href="default.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css">
<link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/main.css">
</head>
<body>
<!-- start header -->
<div id="logo">
	<h1><a href="#">Admin Panale</a></h1>
</div>
<div id="menu">
	<?php include("include\menu.inc.php"); ?>
</div>
<!-- end header -->
<!-- start page -->
<div id="page">
		<!-- start content -->
		<div id="content-admin" > 
		<form action="company_add_process.php" method="post" >
      		<h1>Company details</h1>
		
			<label for="c_name">Name:</label>
			<input type="text" id="c_name" name="c_name">

			<label for="phone_no">Phone No:</label>
			<input type="text" id="phone_no" name="c_phone_no">

			<label for="c_address">Address:</label>
			<textarea type="text" id="c_address" name="c_address"></textarea>

			<label for="c_m_mno">Mobile No:</label>
			<input type="text" id="c_m_mno" name="c_m_mno">
			
			<label for="c_email">Email:</label>
			<input type="email" id="c_email" name="c_email">
			
			<label for="c_gst">GST no:</label>
			<input type="text" id="c_gst" name="c_gst">
						
			<button type="submit">Save </button>
			
			<?php 

				if(isset ($_SESSION['add_company']['ok']))
				{
					echo '<font color="blue">'.($_SESSION['add_company']['ok']).'</font></br>';
				}

				if(!empty($_SESSION['add_company']['error']))
				{
					echo '<font color="blue">'.($_SESSION['add_company']['error']).'</font></br>';
				}
				
				unset($_SESSION['add_company']);
			?>

		</form>			
		</div>
		<!-- end sidebar -->
		<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
<div id="footer">
	<?php include("include/footer.inc.php"); ?>
</div>
</body>
</html>
