<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Admin</title>
<link href="default.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css">
<link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/main.css">
</head>
<body>
<!-- start header -->
<div id="page-container">
	<div id="logo">
		<h1><a href="#">Admin Panale</a></h1>
	</div>

	<div id="menu">
		<?php include("include\menu.inc.php"); ?>
	</div>
	<!-- end header -->
	<!-- start page -->
	<div id="page">
		
		<!-- start content -->
			<div id="content-admin">
				<h1 style="text-align:center;">welcome to admin site</h1>
			</div>
		
		<!-- end content -->
		</div>
	</div>
	<!-- end page -->
	<div id="footer">
		<?php include("include/footer.inc.php"); ?>
	</div>
</div>
</body>
</html>
