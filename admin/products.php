<?php session_start(); 
include_once 'db.php';	
if(!isset($_SESSION['unm']))
{
	header("location:login.php");
}?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Admin</title>
<style type="text/css">
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
<link href="default.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css">
<link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" href="css/main.css">

<script>
$(document).ready(function(){
	$("#viewAll").hide();
	$("#Add").click(function(){
		$("#viewAll").hide();
		$("#myDIV").show();
		
	});
	$("#View").click(function(){
		$("#myDIV").hide();
		$("#viewAll").show();
	});
});
</script>

</head>
<body>
<!-- start header -->
<div id="logo">
	<h1><a href="#">Admin Panale</a></h1>
</div>
<div id="menu">
	<?php include("include\menu.inc.php"); ?>
</div>
<!-- end header -->
<!-- start page -->
<div id="page">
		<!-- start content -->
		<div id="content-admin" > 

		<div style="width: 70px; margin:10px; text-align:left;">
				<button id="Add"  style="padding:5px; !important">Add</button>
				<button id="View" style="padding:5px; !important">View</button>
		</div>

		<div id="myDIV">
			<form action="products_add_process.php" method="post" enctype="multipart/form-data" style="max-width:700px !important;">
				<h1>Products details</h1>

				<table> 
					<tr>
						<td>
							<label for="pr_cpid">Select Company:</label>
							<select name="pr_cpid" style="width:300px;">
							<?php 
								
								$result = mysqli_query($conn,"SELECT * FROM shree_company");
								
								while($row = mysqli_fetch_array($result))
								{
									echo'<option value="'.$row['sh_cid'].'">'.$row['sh_cname'].'</option>';
								}
							?>
							</select>
						</td>
						<td>
							<label for="pr_cate_name">Select Category:</label>
							<select name="pr_cate_name" style="width:300px;">
							<?php 
								
								$result = mysqli_query($conn,"SELECT * FROM products_category");
								
								while($row = mysqli_fetch_array($result))
								{
									echo'<option value="'.$row['pr_cate_name'].'">'.$row['pr_cate_name'].'</option>';
								}
							?>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<label for="pr_name">Name:</label>
							<input type="text" id="pr_name" name="pr_name">
						</td>
					</tr>

					<tr>
						<td colspan="2">
							<label for="pr_description">Product Description:</label>
							<!--<input type="text" id="pr_desc" name="pr_desc">-->
							<textarea placeholder="" id="pr_description" name="pr_description"></textarea>
						</td>
					</tr>

					<tr>
						<td>
							<label for="pr_hsn_code">HSN Code:</label>
							<input type="text" id="pr_hsn_code" name="pr_hsn_code">
						</td>
						<td>
							<label for="pr_type">Product Type:</label>
							<input type="text" id="pr_type" name="pr_type">
						</td>
					</tr>

					<tr>
						<td>
							<label for="pr_gst">GST in (%)</label>
							<select name="pr_gst" style="width:300px;">
							<?php 
								
								$result = mysqli_query($conn,"SELECT * FROM gst_tax");
								
								while($row = mysqli_fetch_array($result))
								{
									echo'<option value="'.$row['gst_percentage'].'">'.$row['gst_name'].'</option>';
								}
							?>
							</select>
						</td>
						<td>
							<label for="pr_price">Product Price:</label>
							<input type="text" id="pr_price" name="pr_price">
						</td>
					</tr>

					<tr>
						<td>
							<label for="pr_quantity">Product Quantiy:</label>
							<input type="text" id="pr_quantity" name="pr_quantity">
						</td>
						<td>
							<label for="file">Uploade Product Image:</label>
							<input type="file" id="file" name="file[]" multiple>
							<?php 
							if(isset($_SESSION['add_products']['file']))
							{
								echo '<font color="red">'.($_SESSION['add_products']['file']).'</font></br>';
							}?>
						</td>
					</tr>

				</table>
				<br/>	
				<button type="submit">Save </button>
				
				<?php 

					if(isset ($_SESSION['add_products']['ok']))
					{
						echo '<font color="blue">'.($_SESSION['add_products']['ok']).'</font></br>';
					}

					if(!empty($_SESSION['add_products']['error']))
					{
						echo '<font color="red">'.($_SESSION['add_products']['error']).'</font></br>';
					}
					
					unset($_SESSION['add_products']);
				?>

			</form>	
		</div>

		<div id="viewAll">

		<table id="customers">
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Category</th>
				<th>HSNCode</th>
				<th>Type</th>
				<th>GST (%)</th>
				<th>Price (RS)</th>
				<th>Quantity</th>
				<th colspan="2">Action</th>
			</tr>
			<?php 
					
				$result = mysqli_query($conn,"SELECT * FROM products");

				$product_count = mysqli_num_rows ($result);

				if ($product_count > 0) {
				
					while($row = mysqli_fetch_array($result)){	
						//echo'<option value="'.$row['sh_cid'].'">'.$row['sh_cname'].'</option>';
						echo'<tr>';
						echo'<td>'.$row['pr_id'].'</td>';
						echo'<td>'.$row['pr_name'].'</td>';
						echo'<td>'.$row['pr_cate_name'].'</td>';
						echo'<td>'.$row['pr_hsn_code'].'</td>';
						echo'<td>'.$row['pr_type'].'</td>';
						echo'<td>'.$row['pr_gst'].'</td>';
						echo'<td>'.$row['pr_price'].'</td>';
						echo'<td>'.$row['pr_quantity'].'</td>';
						//echo'<td>';
						//echo'<input type="button" name="button" style="background: url(./images/edit1.png) no-repeat; width:40px; height:40px; padding: 10px;" class="button"/>';
						//echo'</td>';
						echo'<td>';
						echo'<a href="products_edit.php?id='.$row["pr_id"].'">Edit</a>';
						echo'</td>';
						echo'<td>';
						//echo'<input type="button" name="button" style="background: url(./images/delete.png) no-repeat; width:40px; height:40px; padding: 10px;" class="button"/>';
						echo'<a href="products_edit.php?id='.$row["pr_id"].'">Delete</a>';
						echo'</td>';
						echo'</tr>';
					}
				}
			?>
		</table>

		

		</div>

		</div>
		<!-- end sidebar -->
		<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
<div id="footer">
	<?php include("include/footer.inc.php"); ?>
</div>
</body>
</html>