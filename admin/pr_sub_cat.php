<?php session_start(); 
include_once 'db.php';	
if(!isset($_SESSION['unm']))
{
	header("location:login.php");
}?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Admin</title>
<style type="text/css">

</style>
<link href="default.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css">
<link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/main.css">
</head>
<body>
<!-- start header -->
<div id="logo">
	<h1><a href="#">Admin Panale</a></h1>
</div>
<div id="menu">
	<?php include("include\menu.inc.php"); ?>
</div>
<!-- end header -->
<!-- start page -->
<div id="page">
		<!-- start content -->
		<div id="content-admin" > 
		<form action="pr_sub_cat_add_process.php" method="post" enctype="multipart/form-data">
      		<h1>ADD PRODUCT SUB CATEGORY</h1>
			  <br/>

			<label for="cat">Category Name:</label>
			<input type="text" name="cat">
			<button type="submit">Add</button>
			<?php 
				if(isset ($_SESSION['a_cat']['ok']))
				{
					echo '<font color="blue">'.($_SESSION['a_cat']['ok']).'</font></br>';
				}

				if(!empty($_SESSION['a_cat']['error']))
				{
					echo '<font color="red">'.($_SESSION['a_cat']['error']).'</font></br>';
				}
				
				unset($_SESSION['a_cat']);
			?>
		
		</form>	

		</div>
		<!-- end sidebar -->
		<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
<div id="footer">
	<?php include("include/footer.inc.php"); ?>
</div>
</body>
</html>
