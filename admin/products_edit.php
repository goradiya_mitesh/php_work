<?php session_start(); 
include_once 'db.php';	
if(!isset($_SESSION['unm']))
{
	header("location:login.php");
}

$id=$_REQUEST['id'];
$query = "SELECT * from products where pr_id='".$id."'"; 
$result = mysqli_query($conn, $query) or die ( mysqli_error());
$row = mysqli_fetch_assoc($result);

?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Admin</title>
<style type="text/css">
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
<link href="default.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" href="css/normalize.css">
<link href='https://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link rel="stylesheet" href="css/main.css">

<script>
$(document).ready(function(){
	$("#viewAll").hide();
	$("#Add").click(function(){
		$("#viewAll").hide();
		$("#myDIV").show();
		
	});
	$("#View").click(function(){
		$("#myDIV").hide();
		$("#viewAll").show();
	});
});
</script>

</head>
<body>
<!-- start header -->
<div id="logo">
	<h1><a href="#">Admin Panale</a></h1>
</div>
<div id="menu">
	<?php include("include\menu.inc.php"); ?>
</div>
<!-- end header -->
<!-- start page -->
<div id="page">
		<!-- start content -->
		<div id="content-admin" > 

		<!--<div style="width: 70px; margin:10px; text-align:left;">
				<button id="Add"  style="padding:5px; !important">Add</button>
				<button id="View" style="padding:5px; !important">View</button>
		</div>-->

		<div>
			<form action="products_update_process.php" method="post" enctype="multipart/form-data" style="max-width:700px !important;">
				<h1>Products Update</h1>
				
				<input type="hidden" name="pr_id" value="<?php echo $row['pr_id'];?>" />

				<table> 
					<tr>
						<td>
							<label for="pr_cp_id">Select Company:</label>
							<select name="pr_cp_id" style="width:300px;">
							<?php 
								$result = mysqli_query($conn,"SELECT * FROM shree_company");
								$select_attribute = ''; 								
								while($row1 = mysqli_fetch_array($result))
								{
									if ( $row1['sh_cid'] === $row['pr_cp_id'] ) { 
										$select_attribute = 'selected'; 
									}
									//echo'<option value="'.$row1['sh_cname'].'">'.$row1['sh_cname'].'</option>';
									echo '<option value="'.$row1['sh_cid'].'" '. $select_attribute . ' >'.$row1['sh_cname'].'</option>';
								}
							?>
							</select>
						</td>
						<td>
							<label for="pr_cate_name">Select Category:</label>
							<select name="pr_cate_name" style="width:300px;">
							<?php 
								
								$result = mysqli_query($conn,"SELECT * FROM products_category");

								$select_attribute = ''; 
								
								while($row2 = mysqli_fetch_array($result))
								{
									if ( $row['pr_cate_name'] === $row['pr_cate_name'] ) { 
										$select_attribute = 'selected'; 
									}
									//echo'<option value="'.$row2['pr_cate_name'].'">'.$row2['pr_cate_name'].'</option>';
									echo '<option value="'.$row2['pr_cate_name'].'" '. $select_attribute . ' >'.$row2['pr_cate_name'].'</option>';
								}
							?>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<label for="pr_name">Name:</label>
							<input type="text" id="pr_name" name="pr_name" value="<?php echo $row['pr_name'];?>">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<label for="pr_description">Product Description:</label>
							<!--<input type="text" id="pr_desc" name="pr_desc">-->
							<textarea placeholder="" id="pr_description" name="pr_description"></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<label for="pr_hsn_code">HSN Code:</label>
							<input type="text" id="pr_hsn_code" name="pr_hsn_code" value="<?php echo $row['pr_hsn_code'];?>">
						</td>
						<td>
							<label for="pr_type">Product Type:</label>
							<input type="text" id="pr_type" name="pr_type" value="<?php echo $row['pr_type'];?>">
						</td>
					</tr>
					<tr>
						<td>
							<label for="pr_gst">GST in (%)</label>
							<select name="pr_gst" style="width:300px;" value="<?php echo $row['pr_gst'];?>">
							<?php 
								
								$result = mysqli_query($conn,"SELECT * FROM gst_tax");
								
								while($row3 = mysqli_fetch_array($result))
								{
									echo'<option value="'.$row3['gst_percentage'].'">'.$row3['gst_name'].'</option>';
								}
							?>
							</select>
						</td>
						<td>
							<label for="pr_price">Product Price:</label>
							<input type="text" id="pr_price" name="pr_price" value="<?php echo $row['pr_price'];?>">
						</td>
					</tr>
					<tr>
						<td>
							<label for="pr_quantity">Product Quantiy:</label>
							<input type="text" id="pr_quantity" name="pr_quantity" value="<?php echo $row['pr_quantity'];?>">
						</td>
						<td>
							<label for="file">Uploade Product Image:</label>
							<input type="file" id="file" name="file[]" multiple>

							<?php 
								
								$result = mysqli_query($conn,"SELECT * FROM images where pr_id = '.$id.' ");
								
								while($row4 = mysqli_fetch_array($result))
								{
									echo  $row4['file_name'];
								}
							?>

							<?php 
							if(isset($_SESSION['update_products']['file']))
							{
								echo '<font color="red">'.($_SESSION['update_products']['file']).'</font></br>';
							}?>
						</td>
					</tr>
				</table>
								
				<br/>		
				<br/>	
				<button type="submit">Update</button>
				
				<?php 

					if(isset ($_SESSION['update_products']['ok']))
					{
						echo '<font color="blue">'.($_SESSION['update_products']['ok']).'</font></br>';
					}

					if(!empty($_SESSION['update_products']['error']))
					{
						echo '<font color="red">'.($_SESSION['update_products']['error']).'</font></br>';
					}
					
					unset($_SESSION['update_products']);
				?>

			</form>	
		</div>
					
		</div>
		<!-- end sidebar -->
		<div style="clear: both;">&nbsp;</div>
</div>
<!-- end page -->
<div id="footer">
	<?php include("include/footer.inc.php"); ?>
</div>
</body>
</html>