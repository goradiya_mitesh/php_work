<?PHP session_start();
include("db.php");

	if(!empty($_POST))
	{
		$_SESSION['update_products']=array();
		extract($_POST);
		
		if(empty($pr_name))
		{
			$_SESSION['update_products'][]="please enter sub category name";
			header("location:products_edit.php");	
		}

		else
		{
            $sql = "UPDATE `products` SET `pr_cp_id`=$pr_cpid,`pr_name`=$pr_name,`pr_cate_name`= $pr_cate_name,
            `pr_hsn_code`=$pr_hsn_code,`pr_type`=$pr_type,`pr_gst`=$pr_gst,`pr_price`=$pr_price,`pr_quantity`= $pr_quantity,`pr_description` = $pr_description ,`pr_isdeleted`='0' WHERE `pr_id`= $pr_id";

			//$sql = "INSERT INTO products(pr_cp_id, pr_name, pr_cate_name, pr_hsn_code, pr_type, pr_gst, pr_price, pr_quantity,pr_isdeleted)
			//VALUES ('$pr_cpid','$pr_name','$pr_hsn_code','$pr_type','$pr_gst','$pr_price','$pr_quantity','0')";
				if (mysqli_query($conn, $sql)) {
					$id = mysqli_insert_id($conn);
					
					$targetDir = "./img/";
					$fileName = basename($_FILES["file"]["name"]);
					$targetFilePath = $targetDir . $fileName;
					$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

					if(!empty($_FILES["file"]["name"])){
						// Allow certain file formats
						$allowTypes = array('jpg','png','jpeg','gif','pdf');
						if(in_array($fileType, $allowTypes)){
							// Upload file to server
							if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
								// Insert image file name into database
								$sql = "INSERT into images (pr_id, file_name, uploaded_on) VALUES ('$id','".$fileName."', NOW())";
								if(mysqli_query($conn, $sql)){
									$_SESSION['update_products']['ok'] = "record Uploaded successfully !";
								}else{
									$_SESSION['update_products']['error'] = "File upload failed, please try again.";
								} 
							}else{
								$_SESSION['update_products']['error'] = "Sorry, there was an error uploading your file.";
							}
						}else{
							$_SESSION['update_products']['error'] = 'Sorry, only JPG, JPEG, PNG, GIF, & PDF files are allowed to upload.';
						}
					}else{
						$_SESSION['update_products']['error'] = 'Please select a file to upload.';
					}
				} else {
					$_SESSION['update_products']['error'] = "Error: " . $sql . " " . mysqli_error($conn);
				}
				//mysqli_close($conn);

			header("location:products_edit.php");
		}
	}
	else
	{
	header("location:products_edit.php");
	}
?>