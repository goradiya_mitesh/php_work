<?PHP session_start();
include("db.php");

	if(!empty($_POST))
	{
		$_SESSION['add_products']=array();
		extract($_POST);
		
		if(empty($pr_name))
		{
			$_SESSION['add_products'][]="please enter sub category name";
			header("location:products.php");	
		}

		else
		{
			$sql = "INSERT INTO products(pr_cp_id, pr_name, pr_cate_name, pr_hsn_code, pr_type, pr_gst, pr_price, pr_quantity,pr_description,pr_isdeleted)
			VALUES ('$pr_cpid','$pr_name','$pr_cate_name','$pr_hsn_code','$pr_type','$pr_gst','$pr_price','$pr_quantity','$pr_description','0')";
				if (mysqli_query($conn, $sql)) {
					$id = mysqli_insert_id($conn);
					
					$targetDir = "./img/";
					$countfiles = count($_FILES['file']['name']);

					for($i=0;$i<$countfiles;$i++){
						$fileName = basename($_FILES["file"]["name"][$i]);
						$targetFilePath = $targetDir . $fileName;
						$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);

						if(!empty($_FILES["file"]["name"][$i])){
							// Allow certain file formats
							$allowTypes = array('jpg','png','jpeg','gif','pdf');
							if(in_array($fileType, $allowTypes)){
								// Upload file to server
								if(move_uploaded_file($_FILES["file"]["tmp_name"][$i], $targetFilePath)){
									// Insert image file name into database
									$sql = "INSERT into images (pr_id, file_name, uploaded_on) VALUES ('$id','".$fileName."', NOW())";
									if(mysqli_query($conn, $sql)){
										$_SESSION['add_products']['ok'] = "New record created successfully !";
									}else{
										$_SESSION['add_products']['error'] = "File upload failed, please try again.";
									} 
								}else{
									$_SESSION['add_products']['error'] = "Sorry, there was an error uploading your file.";
								}
							}else{
								$_SESSION['add_products']['error'] = 'Sorry, only JPG, JPEG, PNG, GIF, & PDF files are allowed to upload.';
							}
						}else{
							$_SESSION['add_products']['error'] = 'Please select a file to upload.';
						}
					
					}
					

					
				} else {
					$_SESSION['add_products']['error'] = "Error: " . $sql . " " . mysqli_error($conn);
				}
				//mysqli_close($conn);

			header("location:products.php");
		}
	}
	else
	{
	header("location:products.php");
	}
?>