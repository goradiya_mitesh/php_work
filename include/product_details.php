<?php 
    include_once 'db.php';
?>

<style>
.img_pr{
        width: 100%;
        height: 380px;
       /* display: block;*/ /* remove extra space below image */
       /* object-fit:cover; */
        vertical-align: middle;
}
.box{
    width: 350px;        
    /*border: 2px solid black;*/
}   
</style>
<div class="products">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="section_title text-center">Popular Items</div>
            </div>
        </div>
        <div class="row page_nav_row">
            <div class="col">
                <div class="page_nav">
                    <ul class="d-flex flex-row align-items-start justify-content-center">
                    <?php 					
                        $result = mysqli_query($conn,"SELECT * FROM products_category");
                        
                        while($row = mysqli_fetch_array($result))
                        {
                           // echo'<option value="'.$row['pr_cate_name'].'">'.$row['pr_cate_name'].'</option>';
                            echo'<li><a href="category.html">'.$row['pr_cate_name'].'</a></li>';
                        }
				    ?>
                        <!--<li class="active"><a href="category.html">Women</a></li>
                        <li><a href="category.html">Men</a></li>
                        <li><a href="category.html">Kids</a></li>
                        <li><a href="category.html">Home Deco</a></li>-->
                    </ul>
                </div>
            </div>
        </div>
        <div class="row products_row">
            <?php 					
                $result = mysqli_query($conn,"SELECT * FROM products");
                
                while($row = mysqli_fetch_array($result))
                {
                    // echo'<option value="'.$row['pr_cate_name'].'">'.$row['pr_cate_name'].'</option>';
                    
                        //Product
                        echo'<div class="col-xl-4 col-md-6">';
                        echo'<div class="product">';
                       
                        echo'<div class="box">';
                        $result_img = mysqli_query($conn,"SELECT * FROM images");
                        while($row_img = mysqli_fetch_array($result_img))
                        {
                            if($row_img['pr_id'] == $row['pr_id']){
                                if(strpos($row_img['file_name'], "All") === false){
                                    echo '<img src="admin/img/'.$row_img['file_name'].'" alt="" class="img_pr">';
                                }
                            }
                        }
                        echo '</div>';
                        echo'<div class="product_content">';
                        echo'<div class="product_info d-flex flex-row align-items-start justify-content-start">';
                        echo'<div>';
                        echo'<div>';
                        echo'<div class="product_name"><a href="product.php?id='.$row["pr_id"].'">'.$row['pr_name'].'</a></div>';
                        echo' <div class="product_category">In <a href="category.html">Category</a></div>';
                        echo'</div>';
                        echo'</div>';
                        echo'<div class="ml-auto text-right">';
                        echo'<div class="rating_r rating_r_4 home_item_rating"><i></i><i></i><i></i><i></i><i></i></div>';
                        echo'<div class="product_price text-right">₹'.$row['pr_price'].'</div>'; //<span>.99</span> for pointing value
                        echo'</div>';
                        echo'</div>';
                        echo'<div class="product_buttons">';
                        echo'<div class="text-right d-flex flex-row align-items-start justify-content-start">';
                        echo'<div class="product_button product_fav text-center d-flex flex-column align-items-center justify-content-center">';
                        echo' <div><div><img src="images/heart_2.svg" class="svg" alt=""><div>+</div></div></div>';
                        echo'</div>';
                        echo'<div class="product_button product_cart text-center d-flex flex-column align-items-center justify-content-center">';
                        echo'<div><div><img src="images/cart.svg" class="svg" alt=""><div>+</div></div></div>';
                        echo'</div>';
                        echo'</div>';
                        echo'</div>';
                        echo'</div>';
                        echo'</div>';
                        echo'</div>';
                    
                }
            ?>
           
        </div>

        <!-- Product -->
        <div class="col-xl-4 col-md-6">
                <div class="product">
                    <div class="box"><img src="images/product_6.jpg" alt=""></div>
                    <div class="product_content">
                        <div class="product_info d-flex flex-row align-items-start justify-content-start">
                            <div>
                                <div>
                                    <div class="product_name"><a href="product.html">Cool Clothing with Brown Stripes</a></div>
                                    <div class="product_category">In <a href="category.html">Category</a></div>
                                </div>
                            </div>
                            <div class="ml-auto text-right">
                                <div class="rating_r rating_r_4 home_item_rating"><i></i><i></i><i></i><i></i><i></i></div>
                                <div class="product_price text-right">$3<span>.99</span></div>
                            </div>
                        </div>
                        <div class="product_buttons">
                            <div class="text-right d-flex flex-row align-items-start justify-content-start">
                                <div class="product_button product_fav text-center d-flex flex-column align-items-center justify-content-center">
                                    <div><div><img src="images/heart_2.svg" class="svg" alt=""><div>+</div></div></div>
                                </div>
                                <div class="product_button product_cart text-center d-flex flex-column align-items-center justify-content-center">
                                    <div><div><img src="images/cart.svg" class="svg" alt=""><div>+</div></div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div class="row load_more_row">
            <div class="col">
                <div class="button load_more ml-auto mr-auto"><a href="#">load more</a></div>
            </div>
        </div>
    </div>
</div>