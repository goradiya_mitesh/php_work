<div class="header_overlay"></div>
    <div class="header_content d-flex flex-row align-items-center justify-content-start">
        <div class="logo">
            <a href="index.php">
                <div class="d-flex flex-row align-items-center justify-content-start">
                    <div><img src="images/logo_title.jpg" alt=""></div>
                    <!--<div>Little Closet</div>-->
                </div>
            </a>	
        </div>
        <div class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></div>
       
        <!--<nav class="main_nav">
            <ul class="d-flex flex-row align-items-start justify-content-start">
                <?php //include './include/nav_menu.php'; ?>
            </ul>
        </nav>--> 

        <?php include './include/newnav.php'; ?>

        <div class="header_right d-flex flex-row align-items-center justify-content-start ml-auto">
            <!-- Search -->
            <div class="header_search">
                <form action="#" id="header_search_form">
                    <input type="text" class="search_input" placeholder="Search Item" required="required">
                    <button class="header_search_button"><img src="images/search.png" alt=""></button>
                </form>
            </div>
            <!-- User -->
            <!--<div class="user"><a href="#"><div><img src="images/user.svg" alt="https://www.flaticon.com/authors/freepik"><div>1</div></div></a></div>-->
            <!-- Cart -->
            <div class="cart"><a href="cart.html"><div><img class="svg" src="images/cart.svg" alt="https://www.flaticon.com/authors/freepik"></div></a></div>
            <!-- Phone -->
            <div class="header_phone d-flex flex-row align-items-center justify-content-start">
                <div><div><img src="images/phone.svg" alt="https://www.flaticon.com/authors/freepik"></div></div>
                <div>+91 7622020893</div>
            </div>
        </div>
    </div>