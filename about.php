<!DOCTYPE html>
<html lang="en">
<head>
<title>About Us</title>
<?php include './include/head.php'; ?>
</head>
<style>
.container h1 {
	border-bottom-color: #f44c1c; 
	font-size: 25px;
    font-weight: 700 !important;
}

.container h3 {
	color:#2fce98 !important;
}

.container p {
color:black !important;
}

.container ol li {
	text-align: justify; 
	margin-left:30px;
	font-size: 16px;
	color:black;
}

.container .abus {
	margin-top:50px;
	margin-bottom:50px;
	background: #f8f8f8;
	color: #FFFFFF;
	padding:10px;
}

</style>
<body>

<!-- Menu -->

<div class="menu">
	<?php include './include/menu_mobile.php'; ?>
</div>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<?php include './include/header.php'; ?>
	</header>

	<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->

		<!--<div class="home">-->
			<!-- Home Slider -->
			<?php // include './include/home_slider.php'; ?>
		<!--</div>-->

		<!--container -->

		<div style="margin-top:100px !important; background-color:#2fce98 !important; height:100px; width:100%; color:white; text-align:center; padding:10px;">
			<h3>ABOUT US</h3>	
			<p style="color:white;">If you need to get in touch with us, you can drop by our office during the working hours.</p>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					
					<div class="row">
						<div class="col-md-12 center-column content-with-background abus" id="content">
							<div style="padding-bottom: 10px"><p style="text-align: justify; "><b>Shree Enterprise</b>, an e-commerce shopping platform for personal, professional, general use stationery.&nbsp;This virtual stationery market endeavours to bring a wide variety of products at discounted rates. Being into operation since 2018, the company has shaped a broad mount of satisfied consumers from Schools, Universities, Hospitals, Banks, Corporates, NGO’s and many households.</p>

							<h3>How Can we improve it ?</h3>

							<p style="text-align: justify; ">At <b>Shree Enterprise</b>, we sell multiple stationery products that basic and necessary products are use in our daily life at home, office, school or college by the students and peoples.</p>

							<ol>
								<li>We source our products from all-over India.</li>
								<li>Our products are genuine, attractive, and affordable price.</li>
								<li>We give you 7 day returning facility if any kind of mismatched or damaged product.</li>
								<li>We complement direct office or institutional orders with special bulk discounts.</li>
								<li>We deliver in every nook and corner of India through our courier partners.</li>
							</ol>

							<p style="text-align: justify; ">We constantly keep adding to this petty list of attractions for the only reason to bring you joyous smiles and comfortable shopping. Just to let you know, we keep our inbox logged in throughout the day, even on Sundays. So you are welcome to contact us anytime at <a href="mailto:shree.ent1992@gmail.com"><b>sales@stationeryhut.in</b></a> or <a href="mailto:shree.ent1992@gmail.com"><b>shree.ent1992@gmail.com</b></a>. For you need a telephone conversation, we are open from <b>Monday to Saturday 9 to 6 pm at +91-7622020893</b>.</p>
							</div>
						</div>
							
					</div>
				</div>
			</div>
				
			<div class="row">	
				<div class="col-sm-12">	
				</div>
			</div>
		</div>
		<!-- Footer -->
		<?php include './include/footer.php'; ?>
		
	</div>
		
</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap-4.1.2/popper.js"></script>
<script src="styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/progressbar/progressbar.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>