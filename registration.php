<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Register Now</title>
<?php include './include/head.php'; ?>
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

.input-container {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  width: 100%;
  margin-bottom: 15px;
}

.icon {
  padding: 10px;
  background: #2fce98;
  color: white;
  min-width: 50px;
  text-align: center;
}

.input-field {
  width: 100%;
  padding: 10px;
  outline: none;
}

.input-field:focus {
  border: 2px solid #2fce98;
}

/* Set a style for the submit button */
.btn {
  background-color: #2fce98;
  color: white;
  padding: 15px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.btn:hover {
  opacity: 1;
}

</style>
</head>
<body>

<!-- Menu -->

<div class="menu">
	<?php include './include/menu_mobile.php'; ?>
</div>

<div class="super_container">

	<!-- Header -->

	<header class="header">
		<?php include './include/header.php'; ?>
	</header>

	<div class="super_container_inner">
		<div class="super_overlay"></div>

		<!-- Home -->
		
		<div class="features">
			<div class="container">
				<div class="row">
					
					<!-- Feature -->

					<div class="col-lg-4 feature_col" style="align:center; width:200px;">
							&nbsp;
					</div>

					<div class="col-lg-4 feature_col" style="align:center; width:200px;">
						<form action="add_registration.php" method="post" enctype="multipart/form-data">

								<h2 style="text-align:center;">Create Account</h2>

								<div class="input-container">
									<i class="fa fas fa-user-circle icon" style="font-size:30px" ></i>
									<input class="input-field" type="text" placeholder="Your Name" name="usrnm">
								</div>

								<div class="input-container">
									<i class="fa fa-envelope icon" style="font-size:30px"></i>
									<input class="input-field" type="text" placeholder="Email" name="email">
								</div>

								<div class="input-container">
									<i class="fa fa-mobile-phone icon" style="font-size:30px"></i>
									<input class="input-field" type="text" placeholder="Mobile No" name="mno">
								</div>

								<div class="input-container">
									<i class="fa fa-key icon" style="font-size:30px"></i>
									<input class="input-field" type="password" placeholder="At least 6 characters" name="psw">
								</div>

								<button type="submit" class="btn">Register</button>

								<?php 

									if(isset ($_SESSION['add_registration']['ok']))
									{
										echo '<font color="blue">'.($_SESSION['add_registration']['ok']).'</font></br>';
									}

									if(!empty($_SESSION['add_registration']['error']))
									{
										echo '<font color="red">'.($_SESSION['add_registration']['error']).'</font></br>';
									}
									
									unset($_SESSION['add_registration']);
								?>

							</form>
					</div>

				</div>
			</div>
		</div>

		<!-- Footer -->
		<?php include './include/footer.php'; ?>
		
	</div>
		
</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap-4.1.2/popper.js"></script>
<script src="styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="plugins/greensock/TweenMax.min.js"></script>
<script src="plugins/greensock/TimelineMax.min.js"></script>
<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
<script src="plugins/greensock/animation.gsap.min.js"></script>
<script src="plugins/greensock/ScrollToPlugin.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/progressbar/progressbar.min.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="js/custom.js"></script>
</body>
</html>